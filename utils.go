package main

import (
	"encoding/json"
	"net/http"
)

func respondWithError(w http.ResponseWriter, code int, errcode int, errtype string, message string) {

	err := make(Payload)
	err["code"] = errcode
	err["message"] = message
	err["type"] = errtype

	p := make(Payload)
	p["status"] = "error"
	p["error"] = err

	respondWithJSON(w, code, p)
}

func respond(w http.ResponseWriter, code int, data interface{}) {

	p := make(Payload)
	p["status"] = "OK"
	p["data"] = data
	respondWithJSON(w, code, p)
}

func respondWithJSON(w http.ResponseWriter, code int, payload interface{}) {
	response, _ := json.Marshal(payload)

	w.Header().Set("Content-Type", "application/json")
	w.WriteHeader(code)
	w.Write(response)
}
