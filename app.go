package main

import (
	"log"
	"net/http"

	"github.com/gorilla/mux"
)

type App struct {
	Router *mux.Router
}

// Initialize : init for App
func (a *App) Initialize(user, password, dbname string) {

	a.Router = mux.NewRouter()
	a.initializeRoutes()
}

func (a *App) initializeRoutes() {
	v1 := a.Router.PathPrefix("/api/v1").Subrouter()
	{
		v1.HandleFunc("/version", getVersion).Methods("GET")
	}
}

// Run :  Rest API starter
func (a *App) Run(host string) {
	log.Fatal(http.ListenAndServe(host, a.Router))
}

func getVersion(w http.ResponseWriter, r *http.Request) {

	respond(w, http.StatusOK, "V1")
}
